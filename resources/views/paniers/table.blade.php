<div class="table-responsive">
    <table class="table" id="paniers-table">
        <thead>
            <tr>
                <th>Is Completed</th>
        <th>User Id</th>
        <th>Product Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($paniers as $panier)
            <tr>
                <td>{!! $panier->is_completed !!}</td>
            <td>{!! $panier->user_id !!}</td>
            <td>{!! $panier->product_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['paniers.destroy', $panier->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('paniers.show', [$panier->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('paniers.edit', [$panier->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
