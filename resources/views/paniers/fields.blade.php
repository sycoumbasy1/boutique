<!-- Is Completed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_completed', 'Is Completed:') !!}
    {!! Form::text('is_completed', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    <select name="product_id" class="form-control">
        @if(!$products ->isEmpty())
            @foreach($products as $product)
                <option value={{$product->id}}>{{$product->product_name}}</option>
            @endforeach
        @else
        @endif
    </select>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('paniers.index') !!}" class="btn btn-default">Cancel</a>
</div>