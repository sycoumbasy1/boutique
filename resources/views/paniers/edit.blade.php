@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Panier
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($panier, ['route' => ['paniers.update', $panier->id], 'method' => 'patch']) !!}

                        @include('paniers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection