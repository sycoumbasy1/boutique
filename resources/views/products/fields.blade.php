<!-- Product Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_name', 'Product Name:') !!}
    {!! Form::text('product_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_quantity', 'Product Quantity:') !!}
    {!! Form::text('product_quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_price', 'Product Price:') !!}
    {!! Form::text('product_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Img Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_img_path', 'Product Img Path:') !!}
    {!! Form::file('product_img_path') !!}
</div>
<div class="clearfix"></div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    <select name="category_id" class="form-control">
        @if(!$category_id ->isEmpty())
            @foreach($categories as $category)
                <option value={{$category->id}}>{{$category->category_name}}</option>
            @endforeach
            @else
        @endif
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>
