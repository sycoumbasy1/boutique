<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $product->id !!}</p>
</div>

<!-- Product Name Field -->
<div class="form-group">
    {!! Form::label('product_name', 'Product Name:') !!}
    <p>{!! $product->product_name !!}</p>
</div>

<!-- Product Quantity Field -->
<div class="form-group">
    {!! Form::label('product_quantity', 'Product Quantity:') !!}
    <p>{!! $product->product_quantity !!}</p>
</div>

<!-- Product Price Field -->
<div class="form-group">
    {!! Form::label('product_price', 'Product Price:') !!}
    <p>{!! $product->product_price !!}</p>
</div>

<!-- Product Img Path Field -->
<div class="form-group">
    {!! Form::label('product_img_path', 'Product Img Path:') !!}
    <p>{!! $product->product_img_path !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $product->category_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $product->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $product->updated_at !!}</p>
</div>

