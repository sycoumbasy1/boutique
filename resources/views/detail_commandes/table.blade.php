<div class="table-responsive">
    <table class="table" id="detailCommandes-table">
        <thead>
            <tr>
                <th>Date</th>
        <th>Product Id</th>
        <th>Commande Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($detailCommandes as $detailCommande)
            <tr>
                <td>{!! $detailCommande->date !!}</td>
            <td>{!! $detailCommande->product_id !!}</td>
            <td>{!! $detailCommande->commande_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['detailCommandes.destroy', $detailCommande->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('detailCommandes.show', [$detailCommande->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('detailCommandes.edit', [$detailCommande->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
