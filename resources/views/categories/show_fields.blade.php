<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $category->id !!}</p>
</div>

<!-- Category Name Field -->
<div class="form-group">
    {!! Form::label('category_name', 'Category Name:') !!}
    <p>{!! $category->category_name !!}</p>
</div>

<!-- Product Img Field -->
<div class="form-group">
    {!! Form::label('product_img', 'Product Img:') !!}
    <p>{!! $category->product_img !!}</p>
</div>

<!-- Category Description Field -->
<div class="form-group">
    {!! Form::label('category_description', 'Category Description:') !!}
    <p>{!! $category->category_description !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $category->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $category->updated_at !!}</p>
</div>

