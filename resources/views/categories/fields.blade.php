<!-- Category Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_name', 'Category Name:') !!}
    {!! Form::text('category_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_img', 'Product Img:') !!}
    {!! Form::file('product_img') !!}
</div>
<div class="clearfix"></div>

<!-- Category Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('category_description', 'Category Description:') !!}
    {!! Form::textarea('category_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('categories.index') !!}" class="btn btn-default">Cancel</a>
</div>
