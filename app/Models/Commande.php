<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Commande
 * @package App\Models
 * @version July 21, 2019, 1:56 am UTC
 *
 * @property \App\Models\User user
 * @property integer montant_total
 * @property integer user_id
 */
class Commande extends Model
{
    use SoftDeletes;

    public $table = 'commandes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'montant_total',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'montant_total' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'montant_total' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'detail_commandes', 'commande_id', 'product_id');
    }
}
