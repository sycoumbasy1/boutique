<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version July 21, 2019, 1:52 am UTC
 *
 * @property \App\Models\Category category
 * @property string product_name
 * @property float product_quantity
 * @property integer product_price
 * @property string product_img_path
 * @property integer category_id
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_name',
        'product_quantity',
        'product_price',
        'product_img_path',
        'category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_name' => 'string',
        'product_quantity' => 'float',
        'product_price' => 'integer',
        'product_img_path' => 'string',
        'category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'product_name' => 'required',
        'product_quantity' => 'required',
        'product_price' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categories()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id');
    }
    public function commandes()
    {
        return $this->belongsToMany(\App\Models\Commande::class, 'detail_commandes', 'product_id', 'commande_id');
    }
}
