<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Detail_commande
 * @package App\Models
 * @version July 21, 2019, 2:04 am UTC
 *
 * @property \App\Models\Product product
 * @property \App\Models\Commande commande
 * @property integer date
 * @property integer product_id
 * @property integer commande_id
 */
class Detail_commande extends Model
{
    use SoftDeletes;

    public $table = 'detail_commandes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'date',
        'product_id',
        'commande_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'date' => 'integer',
        'product_id' => 'integer',
        'commande_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function commande()
    {
        return $this->belongsTo(\App\Models\Commande::class, 'commande_id');
    }
}
