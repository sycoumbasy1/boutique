<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Panier
 * @package App\Models
 * @version July 21, 2019, 2:01 am UTC
 *
 * @property \App\Models\User user
 * @property \App\Models\Product product
 * @property boolean is_completed
 * @property integer user_id
 * @property integer product_id
 */
class Panier extends Model
{
    use SoftDeletes;

    public $table = 'paniers';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'is_completed',
        'user_id',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_completed' => 'boolean',
        'user_id' => 'integer',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'is_completed' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_id');
    }
}
