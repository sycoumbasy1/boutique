<?php

namespace App\Repositories;

use App\Models\Commande;
use App\Repositories\BaseRepository;

/**
 * Class CommandeRepository
 * @package App\Repositories
 * @version July 21, 2019, 1:56 am UTC
*/

class CommandeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'montant_total'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commande::class;
    }
}
