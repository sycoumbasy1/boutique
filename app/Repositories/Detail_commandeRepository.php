<?php

namespace App\Repositories;

use App\Models\Detail_commande;
use App\Repositories\BaseRepository;

/**
 * Class Detail_commandeRepository
 * @package App\Repositories
 * @version July 21, 2019, 2:04 am UTC
*/

class Detail_commandeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Detail_commande::class;
    }
}
