<?php

namespace App\Repositories;

use App\Models\Panier;
use App\Repositories\BaseRepository;

/**
 * Class PanierRepository
 * @package App\Repositories
 * @version July 21, 2019, 2:01 am UTC
*/

class PanierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'is_completed'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Panier::class;
    }
}
