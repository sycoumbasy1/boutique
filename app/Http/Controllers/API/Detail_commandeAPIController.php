<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetail_commandeAPIRequest;
use App\Http\Requests\API\UpdateDetail_commandeAPIRequest;
use App\Models\Detail_commande;
use App\Repositories\Detail_commandeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class Detail_commandeController
 * @package App\Http\Controllers\API
 */

class Detail_commandeAPIController extends AppBaseController
{
    /** @var  Detail_commandeRepository */
    private $detailCommandeRepository;

    public function __construct(Detail_commandeRepository $detailCommandeRepo)
    {
        $this->detailCommandeRepository = $detailCommandeRepo;
    }

    /**
     * Display a listing of the Detail_commande.
     * GET|HEAD /detailCommandes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $detailCommandes = $this->detailCommandeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailCommandes->toArray(), 'Detail Commandes retrieved successfully');
    }

    /**
     * Store a newly created Detail_commande in storage.
     * POST /detailCommandes
     *
     * @param CreateDetail_commandeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDetail_commandeAPIRequest $request)
    {
        $input = $request->all();

        $detailCommande = $this->detailCommandeRepository->create($input);

        return $this->sendResponse($detailCommande->toArray(), 'Detail Commande saved successfully');
    }

    /**
     * Display the specified Detail_commande.
     * GET|HEAD /detailCommandes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Detail_commande $detailCommande */
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            return $this->sendError('Detail Commande not found');
        }

        return $this->sendResponse($detailCommande->toArray(), 'Detail Commande retrieved successfully');
    }

    /**
     * Update the specified Detail_commande in storage.
     * PUT/PATCH /detailCommandes/{id}
     *
     * @param int $id
     * @param UpdateDetail_commandeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetail_commandeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Detail_commande $detailCommande */
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            return $this->sendError('Detail Commande not found');
        }

        $detailCommande = $this->detailCommandeRepository->update($input, $id);

        return $this->sendResponse($detailCommande->toArray(), 'Detail_commande updated successfully');
    }

    /**
     * Remove the specified Detail_commande from storage.
     * DELETE /detailCommandes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Detail_commande $detailCommande */
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            return $this->sendError('Detail Commande not found');
        }

        $detailCommande->delete();

        return $this->sendResponse($id, 'Detail Commande deleted successfully');
    }
}
