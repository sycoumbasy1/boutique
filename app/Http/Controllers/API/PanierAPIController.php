<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePanierAPIRequest;
use App\Http\Requests\API\UpdatePanierAPIRequest;
use App\Models\Panier;
use App\Repositories\PanierRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PanierController
 * @package App\Http\Controllers\API
 */

class PanierAPIController extends AppBaseController
{
    /** @var  PanierRepository */
    private $panierRepository;

    public function __construct(PanierRepository $panierRepo)
    {
        $this->panierRepository = $panierRepo;
    }

    /**
     * Display a listing of the Panier.
     * GET|HEAD /paniers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $paniers = $this->panierRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($paniers->toArray(), 'Paniers retrieved successfully');
    }

    /**
     * Store a newly created Panier in storage.
     * POST /paniers
     *
     * @param CreatePanierAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePanierAPIRequest $request)
    {
        $input = $request->all();

        $panier = $this->panierRepository->create($input);

        return $this->sendResponse($panier->toArray(), 'Panier saved successfully');
    }

    /**
     * Display the specified Panier.
     * GET|HEAD /paniers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Panier $panier */
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            return $this->sendError('Panier not found');
        }

        return $this->sendResponse($panier->toArray(), 'Panier retrieved successfully');
    }

    /**
     * Update the specified Panier in storage.
     * PUT/PATCH /paniers/{id}
     *
     * @param int $id
     * @param UpdatePanierAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePanierAPIRequest $request)
    {
        $input = $request->all();

        /** @var Panier $panier */
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            return $this->sendError('Panier not found');
        }

        $panier = $this->panierRepository->update($input, $id);

        return $this->sendResponse($panier->toArray(), 'Panier updated successfully');
    }

    /**
     * Remove the specified Panier from storage.
     * DELETE /paniers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Panier $panier */
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            return $this->sendError('Panier not found');
        }

        $panier->delete();

        return $this->sendResponse($id, 'Panier deleted successfully');
    }
}
