<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePanierRequest;
use App\Http\Requests\UpdatePanierRequest;
use App\Models\Product;
use App\Repositories\PanierRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PanierController extends AppBaseController
{
    /** @var  PanierRepository */
    private $panierRepository;

    public function __construct(PanierRepository $panierRepo)
    {
        $this->panierRepository = $panierRepo;
    }

    /**
     * Display a listing of the Panier.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $paniers = $this->panierRepository->all();

        return view('paniers.index')
            ->with('paniers', $paniers);
    }

    /**
     * Show the form for creating a new Panier.
     *
     * @return Response
     */
    public function create()
    {
        $products = Product::all();
        return view('paniers.create', compact("products"));
    }

    /**
     * Store a newly created Panier in storage.
     *
     * @param CreatePanierRequest $request
     *
     * @return Response
     */
    public function store(CreatePanierRequest $request)
    {
        $input = $request->all();

        $panier = $this->panierRepository->create($input);

        Flash::success('Panier saved successfully.');

        return redirect(route('paniers.index'));
    }

    /**
     * Display the specified Panier.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            Flash::error('Panier not found');

            return redirect(route('paniers.index'));
        }

        return view('paniers.show')->with('panier', $panier);
    }

    /**
     * Show the form for editing the specified Panier.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            Flash::error('Panier not found');

            return redirect(route('paniers.index'));
        }

        return view('paniers.edit')->with('panier', $panier);
    }

    /**
     * Update the specified Panier in storage.
     *
     * @param int $id
     * @param UpdatePanierRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePanierRequest $request)
    {
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            Flash::error('Panier not found');

            return redirect(route('paniers.index'));
        }

        $panier = $this->panierRepository->update($request->all(), $id);

        Flash::success('Panier updated successfully.');

        return redirect(route('paniers.index'));
    }

    /**
     * Remove the specified Panier from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $panier = $this->panierRepository->find($id);

        if (empty($panier)) {
            Flash::error('Panier not found');

            return redirect(route('paniers.index'));
        }

        $this->panierRepository->delete($id);

        Flash::success('Panier deleted successfully.');

        return redirect(route('paniers.index'));
    }
}
