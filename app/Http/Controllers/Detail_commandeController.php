<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDetail_commandeRequest;
use App\Http\Requests\UpdateDetail_commandeRequest;
use App\Repositories\Detail_commandeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Detail_commandeController extends AppBaseController
{
    /** @var  Detail_commandeRepository */
    private $detailCommandeRepository;

    public function __construct(Detail_commandeRepository $detailCommandeRepo)
    {
        $this->detailCommandeRepository = $detailCommandeRepo;
    }

    /**
     * Display a listing of the Detail_commande.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $detailCommandes = $this->detailCommandeRepository->all();

        return view('detail_commandes.index')
            ->with('detailCommandes', $detailCommandes);
    }

    /**
     * Show the form for creating a new Detail_commande.
     *
     * @return Response
     */
    public function create()
    {
        return view('detail_commandes.create');
    }

    /**
     * Store a newly created Detail_commande in storage.
     *
     * @param CreateDetail_commandeRequest $request
     *
     * @return Response
     */
    public function store(CreateDetail_commandeRequest $request)
    {
        $input = $request->all();

        $detailCommande = $this->detailCommandeRepository->create($input);

        Flash::success('Detail Commande saved successfully.');

        return redirect(route('detailCommandes.index'));
    }

    /**
     * Display the specified Detail_commande.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            Flash::error('Detail Commande not found');

            return redirect(route('detailCommandes.index'));
        }

        return view('detail_commandes.show')->with('detailCommande', $detailCommande);
    }

    /**
     * Show the form for editing the specified Detail_commande.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            Flash::error('Detail Commande not found');

            return redirect(route('detailCommandes.index'));
        }

        return view('detail_commandes.edit')->with('detailCommande', $detailCommande);
    }

    /**
     * Update the specified Detail_commande in storage.
     *
     * @param int $id
     * @param UpdateDetail_commandeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetail_commandeRequest $request)
    {
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            Flash::error('Detail Commande not found');

            return redirect(route('detailCommandes.index'));
        }

        $detailCommande = $this->detailCommandeRepository->update($request->all(), $id);

        Flash::success('Detail Commande updated successfully.');

        return redirect(route('detailCommandes.index'));
    }

    /**
     * Remove the specified Detail_commande from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $detailCommande = $this->detailCommandeRepository->find($id);

        if (empty($detailCommande)) {
            Flash::error('Detail Commande not found');

            return redirect(route('detailCommandes.index'));
        }

        $this->detailCommandeRepository->delete($id);

        Flash::success('Detail Commande deleted successfully.');

        return redirect(route('detailCommandes.index'));
    }
}
