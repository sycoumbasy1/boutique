<?php

namespace App\Http\Requests\API;

use App\Models\Detail_commande;
use InfyOm\Generator\Request\APIRequest;

class CreateDetail_commandeAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Detail_commande::$rules;
    }
}
