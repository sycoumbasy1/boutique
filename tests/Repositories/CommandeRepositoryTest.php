<?php namespace Tests\Repositories;

use App\Models\Commande;
use App\Repositories\CommandeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeCommandeTrait;
use Tests\ApiTestTrait;

class CommandeRepositoryTest extends TestCase
{
    use MakeCommandeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CommandeRepository
     */
    protected $commandeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->commandeRepo = \App::make(CommandeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_commande()
    {
        $commande = $this->fakeCommandeData();
        $createdCommande = $this->commandeRepo->create($commande);
        $createdCommande = $createdCommande->toArray();
        $this->assertArrayHasKey('id', $createdCommande);
        $this->assertNotNull($createdCommande['id'], 'Created Commande must have id specified');
        $this->assertNotNull(Commande::find($createdCommande['id']), 'Commande with given id must be in DB');
        $this->assertModelData($commande, $createdCommande);
    }

    /**
     * @test read
     */
    public function test_read_commande()
    {
        $commande = $this->makeCommande();
        $dbCommande = $this->commandeRepo->find($commande->id);
        $dbCommande = $dbCommande->toArray();
        $this->assertModelData($commande->toArray(), $dbCommande);
    }

    /**
     * @test update
     */
    public function test_update_commande()
    {
        $commande = $this->makeCommande();
        $fakeCommande = $this->fakeCommandeData();
        $updatedCommande = $this->commandeRepo->update($fakeCommande, $commande->id);
        $this->assertModelData($fakeCommande, $updatedCommande->toArray());
        $dbCommande = $this->commandeRepo->find($commande->id);
        $this->assertModelData($fakeCommande, $dbCommande->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_commande()
    {
        $commande = $this->makeCommande();
        $resp = $this->commandeRepo->delete($commande->id);
        $this->assertTrue($resp);
        $this->assertNull(Commande::find($commande->id), 'Commande should not exist in DB');
    }
}
