<?php namespace Tests\Repositories;

use App\Models\Detail_commande;
use App\Repositories\Detail_commandeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeDetail_commandeTrait;
use Tests\ApiTestTrait;

class Detail_commandeRepositoryTest extends TestCase
{
    use MakeDetail_commandeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var Detail_commandeRepository
     */
    protected $detailCommandeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailCommandeRepo = \App::make(Detail_commandeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detail_commande()
    {
        $detailCommande = $this->fakeDetail_commandeData();
        $createdDetail_commande = $this->detailCommandeRepo->create($detailCommande);
        $createdDetail_commande = $createdDetail_commande->toArray();
        $this->assertArrayHasKey('id', $createdDetail_commande);
        $this->assertNotNull($createdDetail_commande['id'], 'Created Detail_commande must have id specified');
        $this->assertNotNull(Detail_commande::find($createdDetail_commande['id']), 'Detail_commande with given id must be in DB');
        $this->assertModelData($detailCommande, $createdDetail_commande);
    }

    /**
     * @test read
     */
    public function test_read_detail_commande()
    {
        $detailCommande = $this->makeDetail_commande();
        $dbDetail_commande = $this->detailCommandeRepo->find($detailCommande->id);
        $dbDetail_commande = $dbDetail_commande->toArray();
        $this->assertModelData($detailCommande->toArray(), $dbDetail_commande);
    }

    /**
     * @test update
     */
    public function test_update_detail_commande()
    {
        $detailCommande = $this->makeDetail_commande();
        $fakeDetail_commande = $this->fakeDetail_commandeData();
        $updatedDetail_commande = $this->detailCommandeRepo->update($fakeDetail_commande, $detailCommande->id);
        $this->assertModelData($fakeDetail_commande, $updatedDetail_commande->toArray());
        $dbDetail_commande = $this->detailCommandeRepo->find($detailCommande->id);
        $this->assertModelData($fakeDetail_commande, $dbDetail_commande->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detail_commande()
    {
        $detailCommande = $this->makeDetail_commande();
        $resp = $this->detailCommandeRepo->delete($detailCommande->id);
        $this->assertTrue($resp);
        $this->assertNull(Detail_commande::find($detailCommande->id), 'Detail_commande should not exist in DB');
    }
}
