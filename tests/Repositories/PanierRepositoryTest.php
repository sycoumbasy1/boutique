<?php namespace Tests\Repositories;

use App\Models\Panier;
use App\Repositories\PanierRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakePanierTrait;
use Tests\ApiTestTrait;

class PanierRepositoryTest extends TestCase
{
    use MakePanierTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PanierRepository
     */
    protected $panierRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->panierRepo = \App::make(PanierRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_panier()
    {
        $panier = $this->fakePanierData();
        $createdPanier = $this->panierRepo->create($panier);
        $createdPanier = $createdPanier->toArray();
        $this->assertArrayHasKey('id', $createdPanier);
        $this->assertNotNull($createdPanier['id'], 'Created Panier must have id specified');
        $this->assertNotNull(Panier::find($createdPanier['id']), 'Panier with given id must be in DB');
        $this->assertModelData($panier, $createdPanier);
    }

    /**
     * @test read
     */
    public function test_read_panier()
    {
        $panier = $this->makePanier();
        $dbPanier = $this->panierRepo->find($panier->id);
        $dbPanier = $dbPanier->toArray();
        $this->assertModelData($panier->toArray(), $dbPanier);
    }

    /**
     * @test update
     */
    public function test_update_panier()
    {
        $panier = $this->makePanier();
        $fakePanier = $this->fakePanierData();
        $updatedPanier = $this->panierRepo->update($fakePanier, $panier->id);
        $this->assertModelData($fakePanier, $updatedPanier->toArray());
        $dbPanier = $this->panierRepo->find($panier->id);
        $this->assertModelData($fakePanier, $dbPanier->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_panier()
    {
        $panier = $this->makePanier();
        $resp = $this->panierRepo->delete($panier->id);
        $this->assertTrue($resp);
        $this->assertNull(Panier::find($panier->id), 'Panier should not exist in DB');
    }
}
