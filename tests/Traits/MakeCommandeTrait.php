<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Commande;
use App\Repositories\CommandeRepository;

trait MakeCommandeTrait
{
    /**
     * Create fake instance of Commande and save it in database
     *
     * @param array $commandeFields
     * @return Commande
     */
    public function makeCommande($commandeFields = [])
    {
        /** @var CommandeRepository $commandeRepo */
        $commandeRepo = \App::make(CommandeRepository::class);
        $theme = $this->fakeCommandeData($commandeFields);
        return $commandeRepo->create($theme);
    }

    /**
     * Get fake instance of Commande
     *
     * @param array $commandeFields
     * @return Commande
     */
    public function fakeCommande($commandeFields = [])
    {
        return new Commande($this->fakeCommandeData($commandeFields));
    }

    /**
     * Get fake data of Commande
     *
     * @param array $commandeFields
     * @return array
     */
    public function fakeCommandeData($commandeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'montant_total' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $commandeFields);
    }
}
