<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Detail_commande;
use App\Repositories\Detail_commandeRepository;

trait MakeDetail_commandeTrait
{
    /**
     * Create fake instance of Detail_commande and save it in database
     *
     * @param array $detailCommandeFields
     * @return Detail_commande
     */
    public function makeDetail_commande($detailCommandeFields = [])
    {
        /** @var Detail_commandeRepository $detailCommandeRepo */
        $detailCommandeRepo = \App::make(Detail_commandeRepository::class);
        $theme = $this->fakeDetail_commandeData($detailCommandeFields);
        return $detailCommandeRepo->create($theme);
    }

    /**
     * Get fake instance of Detail_commande
     *
     * @param array $detailCommandeFields
     * @return Detail_commande
     */
    public function fakeDetail_commande($detailCommandeFields = [])
    {
        return new Detail_commande($this->fakeDetail_commandeData($detailCommandeFields));
    }

    /**
     * Get fake data of Detail_commande
     *
     * @param array $detailCommandeFields
     * @return array
     */
    public function fakeDetail_commandeData($detailCommandeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'date' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'commande_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $detailCommandeFields);
    }
}
