<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\Panier;
use App\Repositories\PanierRepository;

trait MakePanierTrait
{
    /**
     * Create fake instance of Panier and save it in database
     *
     * @param array $panierFields
     * @return Panier
     */
    public function makePanier($panierFields = [])
    {
        /** @var PanierRepository $panierRepo */
        $panierRepo = \App::make(PanierRepository::class);
        $theme = $this->fakePanierData($panierFields);
        return $panierRepo->create($theme);
    }

    /**
     * Get fake instance of Panier
     *
     * @param array $panierFields
     * @return Panier
     */
    public function fakePanier($panierFields = [])
    {
        return new Panier($this->fakePanierData($panierFields));
    }

    /**
     * Get fake data of Panier
     *
     * @param array $panierFields
     * @return array
     */
    public function fakePanierData($panierFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'is_completed' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $panierFields);
    }
}
