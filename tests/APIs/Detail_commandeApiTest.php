<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeDetail_commandeTrait;
use Tests\ApiTestTrait;

class Detail_commandeApiTest extends TestCase
{
    use MakeDetail_commandeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detail_commande()
    {
        $detailCommande = $this->fakeDetail_commandeData();
        $this->response = $this->json('POST', '/api/detailCommandes', $detailCommande);

        $this->assertApiResponse($detailCommande);
    }

    /**
     * @test
     */
    public function test_read_detail_commande()
    {
        $detailCommande = $this->makeDetail_commande();
        $this->response = $this->json('GET', '/api/detailCommandes/'.$detailCommande->id);

        $this->assertApiResponse($detailCommande->toArray());
    }

    /**
     * @test
     */
    public function test_update_detail_commande()
    {
        $detailCommande = $this->makeDetail_commande();
        $editedDetail_commande = $this->fakeDetail_commandeData();

        $this->response = $this->json('PUT', '/api/detailCommandes/'.$detailCommande->id, $editedDetail_commande);

        $this->assertApiResponse($editedDetail_commande);
    }

    /**
     * @test
     */
    public function test_delete_detail_commande()
    {
        $detailCommande = $this->makeDetail_commande();
        $this->response = $this->json('DELETE', '/api/detailCommandes/'.$detailCommande->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/detailCommandes/'.$detailCommande->id);

        $this->response->assertStatus(404);
    }
}
