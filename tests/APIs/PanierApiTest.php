<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakePanierTrait;
use Tests\ApiTestTrait;

class PanierApiTest extends TestCase
{
    use MakePanierTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_panier()
    {
        $panier = $this->fakePanierData();
        $this->response = $this->json('POST', '/api/paniers', $panier);

        $this->assertApiResponse($panier);
    }

    /**
     * @test
     */
    public function test_read_panier()
    {
        $panier = $this->makePanier();
        $this->response = $this->json('GET', '/api/paniers/'.$panier->id);

        $this->assertApiResponse($panier->toArray());
    }

    /**
     * @test
     */
    public function test_update_panier()
    {
        $panier = $this->makePanier();
        $editedPanier = $this->fakePanierData();

        $this->response = $this->json('PUT', '/api/paniers/'.$panier->id, $editedPanier);

        $this->assertApiResponse($editedPanier);
    }

    /**
     * @test
     */
    public function test_delete_panier()
    {
        $panier = $this->makePanier();
        $this->response = $this->json('DELETE', '/api/paniers/'.$panier->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/paniers/'.$panier->id);

        $this->response->assertStatus(404);
    }
}
