<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeCommandeTrait;
use Tests\ApiTestTrait;

class CommandeApiTest extends TestCase
{
    use MakeCommandeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_commande()
    {
        $commande = $this->fakeCommandeData();
        $this->response = $this->json('POST', '/api/commandes', $commande);

        $this->assertApiResponse($commande);
    }

    /**
     * @test
     */
    public function test_read_commande()
    {
        $commande = $this->makeCommande();
        $this->response = $this->json('GET', '/api/commandes/'.$commande->id);

        $this->assertApiResponse($commande->toArray());
    }

    /**
     * @test
     */
    public function test_update_commande()
    {
        $commande = $this->makeCommande();
        $editedCommande = $this->fakeCommandeData();

        $this->response = $this->json('PUT', '/api/commandes/'.$commande->id, $editedCommande);

        $this->assertApiResponse($editedCommande);
    }

    /**
     * @test
     */
    public function test_delete_commande()
    {
        $commande = $this->makeCommande();
        $this->response = $this->json('DELETE', '/api/commandes/'.$commande->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/commandes/'.$commande->id);

        $this->response->assertStatus(404);
    }
}
