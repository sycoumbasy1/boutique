<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
Route::post('register', 'API/UserController@register');
Route::post('login', 'API/UserController@authenticate');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {

    Route::get('closed', 'DataController@closed');
    Route::resource('products', 'ProductAPIController');
});
Route::post('user', 'UserController@authenticate');

Route::resource('categories', 'CategoryAPIController');
//Route::post('categories', 'CategoryAPIController@store');

Route::resource('categories', 'CategoryAPIController');

Route::resource('categories', 'CategoryAPIController');



Route::resource('commandes', 'CommandeAPIController');

Route::resource('paniers', 'PanierAPIController');

Route::resource('detail_commandes', 'Detail_commandeAPIController');

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');
Route::get('open', 'DataController@open');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});

Route::get('categories', 'CategoryAPIController@index');
Route::get('categories/{article}', 'CategoryAPIController@show');
Route::post('categories', 'CategoryAPIController@store');
Route::put('categories/{categories}', 'CategoryAPIController@update');
Route::delete('categories/{categories}', 'CategoryAPIController@delete');